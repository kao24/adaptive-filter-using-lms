This project is largely based off code and other files
provided by earlier labs in ECE 420 at the University
of Illinois at Urbana-Champaign in the Spring 2013
semester. The primary files (not including test vectors
and MATLAB files) changed for the purpose of implementing
this project are documented in "filter documentation.txt"
in the root directory of this repository. The actual
files needed to compile and run the program on the
DSP chip are available in the /filter/ folder.

The idea of this project was originally to attain audible
noise cancellation, but due to time constraints, the
scope was reduced to simply implementing an adaptive
filter using the Least Mean Squares (LMS) algorithm.

For more depth on this topic, refer to the file,
"Proposal-LMS.pdf", our project proposal.

This project is targetted towards Texas Instruments'
TI-55x DSP chip, as available in the Communications
Lab in Everitt at UIUC (as of Spring 2013).
