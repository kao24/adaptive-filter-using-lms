function z = load_coef( file_name )
% function z = load_coef( file_name )
%
% An inverse for the save_coef function.
%
% Synopsis: Process a file formated in the following manner (where
% the white-space indicated below can be of any width >= 1):
%
% <beginning of file>
%      .word      <coef1>
%      .word      <coef2>
% ...
%
% <EOF>
%
% The vector z is returned containing the filter coefficients in the
% file.  Each element of z is scaled by 2^-15.

fid = fopen( file_name, 'r' );

if( fid == -1 )
  error( 'file is invalid' );
end

i = 1;

while( ~feof( fid ) )
  s = fscanf( fid, '%s', 1 );
  if( ~strcmp( s, '.word' ) )
    error( 'file is malformed' );
  end
  s = fscanf( fid, '%d' );
  z( i ) = s / 2^15;
  i = i + 1;
end

fclose( fid );