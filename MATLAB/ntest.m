% compares the exported signal output from the dsp (saved as output.dat) to MATLAB's NLMS

%% initialize
close all;
clear all;
clc;

stepsize = 6553;

d1 = load_coef('d.txt');
x1 = load_coef('x.txt');

% take only every other
d = zeros(1,size(d1,2)/2);
x = zeros(1,size(x1,2)/2);
for j=1:(size(d1,2)/2)
    d(j) = d1(2*j-1);
    x(j) = x1(2*j-1);
end

%% Compute MATLAB's LMS output
% this stepsize should be the same as at the top of main.c
ha = adaptfilt.nlms(8,stepsize/32768); %filterorder, stepsize
[ny,ne] = filter(ha,x,d);

%% Read results from DSP
A = read_vector_mod('output.dat');

%% compare results
figure;
subplot(4,1,1);plot(A);
title('our result (blue) vs. matlab (red)');
% hold on;plot(ny, 'r');

% subplot(4,1,2);plot(ny - A,'r');
% title('Difference between matlab LMS and our DSP LMS');
subplot(4,1,2);plot(ny,'r');
title('matlab NLMS');

subplot(4,1,3);plot(d, 'g');
title('desired');

subplot(4,1,4);plot(d - A);
title('Difference between desired and our DSP LMS');