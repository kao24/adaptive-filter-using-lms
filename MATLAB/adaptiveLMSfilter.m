% IDK. hao wrote this.

function [hfilter,output,e]=adaptiveLMSfilter(N,stepsize,filterorder,x,d)
close all;


%% obtain adapted filter
h=zeros(filterorder,1);
e=zeros(N,1);

%get h(i)
h(1)=e(1)*stepsize;
xbuf=fliplr(x(1:filterorder))';

for i=1:N-(filterorder+1)
    
    %Calculate the weight for the normalized LMS alogrithm

    
    xbuf=[x(i); xbuf(1:filterorder-1)];
    y(i)=h'*xbuf; 
    e(i)=d(i)-y(i);
    
    h=e(i)*stepsize*xbuf+h;
   
end;

%% matlab's LMS algorithm
ha = adaptfilt.lms(filterorder+1, stepsize);
[lms, lmse] = filter(ha, x, d);
max(lmse)
min(lmse)



%Set return value
e=e;
output=y;
hfilter=h;


figure;
subplot(2,1,1);
plot(e);
title('Error with our adaptive NLMS Implementation, Filterorder = 512, Stepsize= 0.026');
ylabel('Error');
xlabel('n');


subplot(2,1,2);
plot(lmse);
title('Error with MATLAB Adaptive NLMS Implementation,Filterorder = 512, Stepsize= 0.026');
xlabel('n')
ylabel('Error')
axis([1, N, -0.05 0.05]);







figure;
plot(hfilter);
title('Filter Coefficents Generated using Our Adaptive LMS Implementation in Code Composer');
xlabel('Filterorder');
ylabel('Filter Coefficent');

%axis([1, N, -0.5 0.5]);





end