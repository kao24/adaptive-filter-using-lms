% generate a random signal x of length N (defined below) and filter it through an IIR filter 
% to obtain the desired signal d (also of length N). save these signals into two different 
% files, x.txt and d.txt, which can then be imported into assembly code as test vectors

%% Generate x (original noise, without filtering)
%code for generate test vector
N=96000;
x=rand(1,N)-0.5;
[B,A] = ellip(4,.25,10,.25);
[hn,w]=freqz(B,A);

%save coefficent  x.txt is the file with ".word"  and xfixed.txt is the one without ".word". Values are exactly same
%we will read xfixed and run the Matlab adaptive lms function to compare result
save_coef('x.txt', x);  

%% Generate d (desired)
d = filter(B, A, x);
d=d(1:N);
save_coef('d.txt', d);