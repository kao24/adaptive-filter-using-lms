/*   CSL configuration is no longer supported.    */
/*   Your CSL configuration has been saved here as part  */
/*   of the process of converting cdb to tcf.    */

/* INPUT dsk5510_dual3006.cdb */

#define CHIP_5510PG2_2 1

/*  Include Header Files  */
#include <csl_emif.h>
#include <csl_mcbsp.h>

#ifdef __cplusplus
extern "C" {
#endif

extern EMIF_Config emifCfg0;
extern MCBSP_Config IISm;
extern MCBSP_Config IISs;
extern MCBSP_Handle hMcbsp0;
extern MCBSP_Handle hMcbsp1;
extern void cslCfgInit();

#ifdef __cplusplus
}
#endif /* extern "C" */
