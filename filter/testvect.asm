     .global _tv_inbuf	; x
     .global _tv_outbuf
     .global _tv_inbuf1	; d

     .copy "macro.asm"
     .sect ".etext"

_tv_outbuf
	;each address is 16 bits, we have 1000 samples to output
     .space 16*1000

	;copy _tv_inbuf and _tv_inbuf_1 here
_tv_inbuf
	.copy "x.txt"

_tv_inbuf1
	.copy "d.txt"
