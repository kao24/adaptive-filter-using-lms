################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
TCF_SRCS += \
../dsk5510_dual3006.tcf 

LIB_SRCS += \
C:/ti/dsk5510/lib/dsk5510bslx.lib 

ASM_SRCS += \
../coef.asm \
../filtercode.asm \
../macro.asm 

S??_SRCS += \
./dsk5510_dual3006cfg.s?? 

C_SRCS += \
./dsk5510_dual3006cfg_c.c \
../dsk5510_dual3006cfg_csl.c \
../main.c 

OBJS += \
./coef.obj \
./dsk5510_dual3006cfg.obj \
./dsk5510_dual3006cfg_c.obj \
./dsk5510_dual3006cfg_csl.obj \
./filtercode.obj \
./macro.obj \
./main.obj 

GEN_SRCS += \
./dsk5510_dual3006cfg.cmd \
./dsk5510_dual3006cfg.s?? \
./dsk5510_dual3006cfg_c.c 

GEN_MISC_FILES += \
./dsk5510_dual3006cfg.h \
./dsk5510_dual3006cfg.h?? \
./dsk5510_dual3006.cdb 

ASM_DEPS += \
./coef.pp \
./filtercode.pp \
./macro.pp 

S??_DEPS += \
./dsk5510_dual3006cfg.pp 

C_DEPS += \
./dsk5510_dual3006cfg_c.pp \
./dsk5510_dual3006cfg_csl.pp \
./main.pp 

GEN_CMDS += \
./dsk5510_dual3006cfg.cmd 

GEN_SRCS__QUOTED += \
"dsk5510_dual3006cfg.cmd" \
"dsk5510_dual3006cfg.s??" \
"dsk5510_dual3006cfg_c.c" 

GEN_MISC_FILES__QUOTED += \
"dsk5510_dual3006cfg.h" \
"dsk5510_dual3006cfg.h??" \
"dsk5510_dual3006.cdb" 

C_DEPS__QUOTED += \
"dsk5510_dual3006cfg_c.pp" \
"dsk5510_dual3006cfg_csl.pp" \
"main.pp" 

S??_DEPS__QUOTED += \
"dsk5510_dual3006cfg.pp" 

OBJS__QUOTED += \
"coef.obj" \
"dsk5510_dual3006cfg.obj" \
"dsk5510_dual3006cfg_c.obj" \
"dsk5510_dual3006cfg_csl.obj" \
"filtercode.obj" \
"macro.obj" \
"main.obj" 

ASM_DEPS__QUOTED += \
"coef.pp" \
"filtercode.pp" \
"macro.pp" 

ASM_SRCS__QUOTED += \
"../coef.asm" \
"../filtercode.asm" \
"../macro.asm" 

TCF_SRCS__QUOTED += \
"../dsk5510_dual3006.tcf" 

GEN_CMDS__FLAG += \
-l"./dsk5510_dual3006cfg.cmd" 

S??_SRCS__QUOTED += \
"./dsk5510_dual3006cfg.s??" 

S??_OBJS__QUOTED += \
"dsk5510_dual3006cfg.obj" 

C_SRCS__QUOTED += \
"./dsk5510_dual3006cfg_c.c" \
"../dsk5510_dual3006cfg_csl.c" \
"../main.c" 


