################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LIB_SRCS += \
C:/Program\ Files/Texas\ Instruments/dsk5510/lib/dsk5510bslx.lib 

C_SRCS += \
../dsk5510_dual3006cfg_csl.c \
../main.c 

TCF_SRCS += \
../dsk5510_dual3006.tcf 

ASM_SRCS += \
../filtercode.asm 

GEN_SRCS += \
./dsk5510_dual3006cfg.cmd \
./dsk5510_dual3006cfg.s?? \
./dsk5510_dual3006cfg_c.c 

GEN_CMDS += \
./dsk5510_dual3006cfg.cmd 

ASM_DEPS += \
./filtercode.pp 

OBJS += \
./dsk5510_dual3006cfg.obj \
./dsk5510_dual3006cfg_c.obj \
./dsk5510_dual3006cfg_csl.obj \
./filtercode.obj \
./main.obj 

S??_DEPS += \
./dsk5510_dual3006cfg.pp 

C_DEPS += \
./dsk5510_dual3006cfg_c.pp \
./dsk5510_dual3006cfg_csl.pp \
./main.pp 

TCF_SRCS_QUOTED += \
"../dsk5510_dual3006.tcf" 

GEN_CMDS_QUOTED += \
-l"./dsk5510_dual3006cfg.cmd" 

C_SRCS_QUOTED += \
"../dsk5510_dual3006cfg_csl.c" \
"../main.c" 

ASM_SRCS_QUOTED += \
"../filtercode.asm" 


# Each subdirectory must supply rules for building sources it contributes
dsk5510_dual3006cfg.cmd: ../dsk5510_dual3006.tcf
	@echo 'Building file: $<'
	@echo 'Invoking: TConf Script Compiler'
	"C:/Program Files/Texas Instruments/xdctools_3_15_02_62/tconf" -b -Dconfig.importPath="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages"; "$<"
	@echo 'Finished building: $<'
	@echo ' '

dsk5510_dual3006cfg.s??: dsk5510_dual3006cfg.cmd
dsk5510_dual3006cfg_c.c: dsk5510_dual3006cfg.cmd

./dsk5510_dual3006cfg.obj: ./dsk5510_dual3006cfg.s?? $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C5500 Compiler'
	"C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/bin/cl55" -g --define="_DEBUG" --include_path="C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/include" --include_path="C:/Program Files/Texas Instruments/C55xCSL/include" --include_path="C:/Users/jmatz/Documents/TI/filter/Debug" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/bios/include" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/rtdx/include/c5500" --include_path="C:/Program Files/Texas Instruments/xdais_6_23/packages/ti/xdais" --include_path="/include" --include_path="C:/Program Files/Texas Instruments/dsk5510/include" --diag_warning=225 --sat_reassoc=off --large_memory_model --ptrdiff_size=32 --fp_reassoc=off --asm_source=mnemonic --preproc_with_compile --preproc_dependency="dsk5510_dual3006cfg.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

./dsk5510_dual3006cfg_c.obj: ./dsk5510_dual3006cfg_c.c $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C5500 Compiler'
	"C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/bin/cl55" -g --define="_DEBUG" --include_path="C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/include" --include_path="C:/Program Files/Texas Instruments/C55xCSL/include" --include_path="C:/Users/jmatz/Documents/TI/filter/Debug" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/bios/include" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/rtdx/include/c5500" --include_path="C:/Program Files/Texas Instruments/xdais_6_23/packages/ti/xdais" --include_path="/include" --include_path="C:/Program Files/Texas Instruments/dsk5510/include" --diag_warning=225 --sat_reassoc=off --large_memory_model --ptrdiff_size=32 --fp_reassoc=off --asm_source=mnemonic --preproc_with_compile --preproc_dependency="dsk5510_dual3006cfg_c.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

./dsk5510_dual3006cfg_csl.obj: ../dsk5510_dual3006cfg_csl.c $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C5500 Compiler'
	"C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/bin/cl55" -g --define="_DEBUG" --include_path="C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/include" --include_path="C:/Program Files/Texas Instruments/C55xCSL/include" --include_path="C:/Users/jmatz/Documents/TI/filter/Debug" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/bios/include" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/rtdx/include/c5500" --include_path="C:/Program Files/Texas Instruments/xdais_6_23/packages/ti/xdais" --include_path="/include" --include_path="C:/Program Files/Texas Instruments/dsk5510/include" --diag_warning=225 --sat_reassoc=off --large_memory_model --ptrdiff_size=32 --fp_reassoc=off --asm_source=mnemonic --preproc_with_compile --preproc_dependency="dsk5510_dual3006cfg_csl.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

./filtercode.obj: ../filtercode.asm $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C5500 Compiler'
	"C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/bin/cl55" -g --define="_DEBUG" --include_path="C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/include" --include_path="C:/Program Files/Texas Instruments/C55xCSL/include" --include_path="C:/Users/jmatz/Documents/TI/filter/Debug" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/bios/include" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/rtdx/include/c5500" --include_path="C:/Program Files/Texas Instruments/xdais_6_23/packages/ti/xdais" --include_path="/include" --include_path="C:/Program Files/Texas Instruments/dsk5510/include" --diag_warning=225 --sat_reassoc=off --large_memory_model --ptrdiff_size=32 --fp_reassoc=off --asm_source=mnemonic --preproc_with_compile --preproc_dependency="filtercode.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '

./main.obj: ../main.c $(GEN_SRCS) $(GEN_OPTS)
	@echo 'Building file: $<'
	@echo 'Invoking: C5500 Compiler'
	"C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/bin/cl55" -g --define="_DEBUG" --include_path="C:/Program Files/Texas Instruments/ccsv4/tools/compiler/c5500/include" --include_path="C:/Program Files/Texas Instruments/C55xCSL/include" --include_path="C:/Users/jmatz/Documents/TI/filter/Debug" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/bios/include" --include_path="C:/Program Files/Texas Instruments/bios_5_40_02_22/packages/ti/rtdx/include/c5500" --include_path="C:/Program Files/Texas Instruments/xdais_6_23/packages/ti/xdais" --include_path="/include" --include_path="C:/Program Files/Texas Instruments/dsk5510/include" --diag_warning=225 --sat_reassoc=off --large_memory_model --ptrdiff_size=32 --fp_reassoc=off --asm_source=mnemonic --preproc_with_compile --preproc_dependency="main.pp" $(GEN_OPTS_QUOTED) $(subst #,$(wildcard $(subst $(SPACE),\$(SPACE),$<)),"#")
	@echo 'Finished building: $<'
	@echo ' '


