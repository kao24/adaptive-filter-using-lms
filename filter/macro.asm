ENTER_ASM	.macro
	PSHM	T2
	PSHM	T3
	PSHM	AR5
	PSHM	AR6
	PSHM	AR7
	PSHM	ST0_55
	PSHM	ST1_55
	PSHM	ST2_55
	PSHM	ST3_55
	AND	#001FFh, mmap(ST0_55)		;clear all ACOVx,TC1, TC2, C
	OR	#04140h, mmap(ST1_55)		;set CPL, SXMD, FRCT
	AND	#0F9DFh, mmap(ST1_55)		;clear M40, SATD, 54CM    
	AND	#07A00h, mmap(ST2_55)		;clear ARMS, RDM, CDPLC, AR[0-7]LC
	AND	#0FFDDh, mmap(ST3_55)		;clear SATA, SMUL
	.endm

LEAVE_ASM	.macro
	BCLR	FRCT			        ;clear FRCT
	AND	#0FE00h, mmap(ST2_55)	    ;clear CDPLC and AR[7-0]LC
	BSET	ARMS			        ;set ARMS
	POPM	ST3_55
	POPM	ST2_55
	POPM	ST1_55
	POPM	ST0_55
	POPM	AR7
	POPM	AR6
	POPM	AR5
	POPM	T3
	POPM	T2
	.endm
