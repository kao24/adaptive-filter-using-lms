/*   CSL configuration is no longer supported.    */
/*   Your CSL configuration has been saved here as part  */
/*   of the process of converting cdb to tcf.    */

/* INPUT dsk5510_dual3006.cdb */

/*  Include Header File  */
#include "dsk5510_dual3006cfg_csl.h"

/*
 * The following steps are required to use this file:
 *
 * 1.  Modify any source files that use these structures to
 *     `#include <dsk5510_dual3006cfg_csl.h>`.  These structure definitions were
 *     previously contained in <dsk5510_dual3006cfg.h>.
 * 2.  Call `CSL_init()` and `cslCfgInit()` from your application's main()
 *     function.  This is necessary to initialize CSL and the assorted
 *     CSL handles.  These functions were previously called by BIOS internally.
 * 3.  Remove the #error line above.
 * 4.  Update your linker options to include the CSL library for the device
 *     that you are using. This library name was listed in the original
 *     generated dsk5510_dual3006cfg.cmd file (e.g., "-lcsl6416.lib").
 * 5.  If you are using the 54x or 55x, you will need to create an additional
 *     linker .cmd file that includes the generated dsk5510_dual3006cfg.cmd file
 *     and adds section placement for the '.csldata' section.
 *
 * See the DSP/BIOS Setup Guide for more information.
 */

#pragma CODE_SECTION(cslCfgInit,".text:cslCfgInit")

/*  Config Structures */

/* EMIF_Config emifCfg0
 * comment                                                 <add comments here>
 * Memory Type                                             32-bit wide SDRAM
 * Read Setup Width                                        0xf
 * Write Setup Width                                       0xf
 * Read Strobe Width                                       0x3f
 * Write Strobe Width                                      0x3f
 * Read Hold Width                                         0x3
 * Write Hold Width                                        0x3
 * Extended Read Hold Width                                0x1
 * Extended Write Hold Width                               0x1
 * Length of Bus Error Timeout                             0x00
 * Memory Type                                             16-bit wide Asynchronous
 * Read Setup Width                                        0x0
 * Write Setup Width                                       0x0
 * Read Strobe Width                                       0x0e
 * Write Strobe Width                                      0x0e
 * Read Hold Width                                         0x0
 * Write Hold Width                                        0x0
 * Extended Read Hold Width                                0x0
 * Extended Write Hold Width                               0x0
 * Length of Bus Error Timeout                             0x00
 * Memory Type                                             16-bit wide Asynchronous
 * Read Setup Width                                        0x0
 * Write Setup Width                                       0x0
 * Read Strobe Width                                       0x14
 * Write Strobe Width                                      0x14
 * Read Hold Width                                         0x0
 * Write Hold Width                                        0x0
 * Extended Read Hold Width                                0x0
 * Extended Write Hold Width                               0x0
 * Length of Bus Error Timeout                             0x00
 * Memory Type                                             16-bit wide Asynchronous
 * Read Setup Width                                        0x0
 * Write Setup Width                                       0x0
 * Read Strobe Width                                       0x14
 * Write Strobe Width                                      0x14
 * Read Hold Width                                         0x0
 * Write Hold Width                                        0x0
 * Extended Read Hold Width                                0x0
 * Extended Write Hold Width                               0x0
 * Length of Bus Error Timeout                             0x00
 * Reset EMIF State Machine                                0x1
 * Memory Clock Enable                                     CLKMEM Enabled to Clock
 * Memory Clock Frequency                                  CLKMEM = DSP Clock / 2
 * External HOLD Disable                                   Hold Disabled
 * Write Posting Enable                                    Disabled
 * Initialize SDRAM                                        0x1
 * SDRAM Data Bus Interface Width                          32-bits wide (D[31:0])
 * SDRAM Size Select                                       64Mbit
 * SDRAM Width Select                                      32-bits
 * Refresh Enable                                          Enabled
 * Refresh Period                                          0x578
 * TRP Value [(Trp /SDCLK) - 1]                            0x1
 * TRC Value [(Trc /SDCLK) - 1]                            0x05
 * TRCD Value [(Trcd /SDCLK) - 1]                          0x1
 * TRAS Value [(Tras /SDCLK) - 1]                          0x3
 * TMRD Value [(Tmrd /SDCLK) - 1]                          0x1
 * Delay from ACTV to ACTV Command [(Tactv2actv /SDCLK) -  0x5
 * Configure Manually                                      0x0
 * Global Control Register                                 0x0221
 * Global Reset Register                                   0xffff
 * CE0 Space Control Register 1                            0x3fff
 * CE0 Space Control Register 2                            0x5fff
 * CE0 Space Control Register 3                            0x0000
 * CE1 Space Control Register 1                            0x1038
 * CE1 Space Control Register 2                            0x0038
 * CE1 Space Control Register 3                            0x0000
 * CE2 Space Control Register 1                            0x1050
 * CE2 Space Control Register 2                            0x0050
 * CE2 Space Control Register 3                            0x0000
 * CE3 Space Control Register 1                            0x1050
 * CE3 Space Control Register 2                            0x0050
 * CE3 Space Control Register 3                            0x0000
 * SDRAM Control Register 1                                0x2b11
 * SDRAM Period Register                                   0x0578
 * SDRAM Initialization Register                           0xffff
 * SDRAM Control Register 2                                0x0535
 */
EMIF_Config emifCfg0 = {
    0x0221,        /*  Global Control Register   */
    0xffff,        /*  Global Reset Register   */
    0x3fff,        /*  CE0 Space Control Register 1   */
    0x5fff,        /*  CE0 Space Control Register 2   */
    0x0000,        /*  CE0 Space Control Register 3   */
    0x1038,        /*  CE1 Space Control Register 1   */
    0x0038,        /*  CE1 Space Control Register 2   */
    0x0000,        /*  CE1 Space Control Register 3   */
    0x1050,        /*  CE2 Space Control Register 1   */
    0x0050,        /*  CE2 Space Control Register 2   */
    0x0000,        /*  CE2 Space Control Register 3   */
    0x1050,        /*  CE3 Space Control Register 1   */
    0x0050,        /*  CE3 Space Control Register 2   */
    0x0000,        /*  CE3 Space Control Register 3   */
    0x2b11,        /*  SDRAM Control Register 1   */
    0x0578,        /*  SDRAM Period Register   */
    0xffff,        /*  SDRAM Initialization Register   */
    0x0535         /*  SDRAM Control Register 2   */
};

/* MCBSP_Config IISm
 * comment                                                 IIS Master Mode
 * Stop all clocks in McBSP during Peripheral Idle Mode    0x0
 * Configure DX, FSX, and CLKX as Serial Port Pins         0x1
 * Clock Mode (CLKXM)                                      Internal
 * SPI Clock Mode (CLKXM)                                  Master
 * Clock Polarity (CLKXP)                                  Falling Edge
 * Frame-Sync Polarity (FSXP)                              Active High
 * DX Pin Delay (DXENA)                                    Disable
 * Transmit Delay (XDATDLY)                                0-bit
 * Phase (XPHASE)                                          Single-phase
 * Word Length Phase1 (XWDLEN1)                            32-bits
 * Word Length Phase2 (XWDLEN2)                            8-bits
 * Words/Frame Phase1 (XFRLEN1)                            0x1
 * Words/Frame Phase2 (XFRLEN2)                            0x1
 * Detect Sync Error (XSYNCERR)                            Disable
 * Interrupt Mode (XINTM)                                  XRDY
 * Early Frame Sync Response (XFIG)                        Ignore
 * Companding (XCOMPAND)                                   No Companding-MSB First
 * Transmit Frame-Sync Source                              FSG Signal
 * Configure DR, FSR, CLKR, and CLKS as Serial Port Pins   0x1
 * Clock Mode (CLKRM)                                      Internal
 * SPI Clock Mode (CLKRM)                                  CLKR as Output
 * Clock Polarity (CLKRP)                                  Rising Edge
 * Frame-Sync Polarity (FSRP)                              Active High
 * Receive Delay (RDATDLY)                                 0-bit
 * Phases (RPHASE)                                         Single-phase
 * Word Length Phase1 (RWDLEN1)                            32-bits
 * Word Length Phase2 (RWDLEN2)                            8-bits
 * Words/Frame Phase1 (RFRLEN1)                            0x2
 * Words/Frame Phase2 (RFRLEN2)                            0x1
 * Detect Sync Error (RSYNCERR)                            Disable
 * Interrupt Mode (RINTM)                                  RRDY
 * Frame-Sync Mode (FSRM)                                  External
 * Early Frame Sync Response (RFIG)                        Restart Transfer
 * Sign-Ext and Justification (RJUST)                      Right-justify/zero-fill
 * Companding (RCOMPAND)                                   No Companding-MSB First
 * Breakpoint Emulation                                    Stop After Current Word
 * SPI Mode (CLKSTP)                                       Disable
 * Digital Loop Back (DLB)                                 Disable
 * A-Bis (ABIS)                                            Disable
 * SRG Clock Source (CLKSM)                                CLKS Pin
 * Clock Synchronization With CLKS Pin (GSYNC)             Disable
 * CLKS Polarity Clock Edge (From CLKS Pin) (CLKSP)        Rising Edge of CLKS
 * Transmit Frame-Sync Mode (FSXM = 1) (FSGM)              Enable
 * Frame Width (1-256) (FWID)                              0x10
 * Clock Divider (1-256) (CLKGDV)                          0x8
 * Frame Period (1-4096) (FPER)                            0x20
 * Select CLKX Pin as                                      Input
 * Select FSX Pin as                                       Input
 * Select CLKR Pin as                                      Input
 * Select FSR Pin as                                       Input
 * Select DX Pin as                                        Output
 * Select DR Pin as                                        Input
 * Select CLKS Pin as                                      Input
 * Set Manually                                            0x0
 * Serial Port Control Register 1                          0x0000
 * Serial Port Control Register 2                          0x0100
 * Receive Control Register 1                              0x01a0
 * Receive Control Register 2                              0x0000
 * Transmit Control Register 1                             0x00a0
 * Transmit Control Register 2                             0x0004
 * Sample Rate Generator Register 1                        0x0f07
 * Sample Rate Generator Register 2                        0x101f
 * Multichannel Control Register 1                         0x0000
 * Multichannel Control Register 2                         0x0000
 * Pin Control Register                                    0x0b03
 * RX Channel Enable                                       All 128 Channels
 * Receive Partition A (RPABLK)                            Block 0. Channel 0-15
 * Receive Partition B (RPBBLK)                            Block 1. Channel 16-31
 * Receive Channel Enable Register Partition A             0x0000
 * Receive Channel Enable Register Partition B             0x0000
 * Receive Channel Enable Register Partition C             0x0000
 * Receive Channel Enable Register Partition D             0x0000
 * Receive Channel Enable Register Partition E             0x0000
 * Receive Channel Enable Register Partition F             0x0000
 * Receive Channel Enable Register Partition G             0x0000
 * Receive Channel Enable Register Partition H             0x0000
 * TX Channel Enable                                       All 128 Channels
 * Transmit Partition A (XPABLK)                           Block 0. Channel 0-15
 * Transmit Partition B (XPBBLK)                           Block 1. Channel 16-31
 * Transmit Channel Enable Register Partition A            0x0000
 * Transmit Channel Enable Register Partition B            0x0000
 * Transmit Channel Enable Register Partition C            0x0000
 * Transmit Channel Enable Register Partition D            0x0000
 * Transmit Channel Enable Register Partition E            0x0000
 * Transmit Channel Enable Register Partition F            0x0000
 * Transmit Channel Enable Register Partition G            0x0000
 * Transmit Channel Enable Register Partition H            0x0000
 * ABIS RX. Bit Enable Register Partition A                0x0000
 * ABIS RX. Bit Enable Register Partition B                0x0000
 * ABIS TX. Bit Enable Register Partition A                0x0000
 * ABIS TX. Bit Enable Register Partition B                0x0000
 */
MCBSP_Config IISm = {
    0x0000,        /*  Serial Port Control Register 1   */
    0x0100,        /*  Serial Port Control Register 2   */
    0x01a0,        /*  Receive Control Register 1   */
    0x0000,        /*  Receive Control Register 2   */
    0x00a0,        /*  Transmit Control Register 1   */
    0x0004,        /*  Transmit Control Register 2   */
    0x0f07,        /*  Sample Rate Generator Register 1   */
    0x101f,        /*  Sample Rate Generator Register 2   */
    0x0000,        /*  Multichannel Control Register 1   */
    0x0000,        /*  Multichannel Control Register 2   */
    0x0b03,        /*  Pin Control Register   */
    0x0000,        /*  Receive Channel Enable Register Partition A   */
    0x0000,        /*  Receive Channel Enable Register Partition B   */
    0x0000,        /*  Receive Channel Enable Register Partition C   */
    0x0000,        /*  Receive Channel Enable Register Partition D   */
    0x0000,        /*  Receive Channel Enable Register Partition E   */
    0x0000,        /*  Receive Channel Enable Register Partition F   */
    0x0000,        /*  Receive Channel Enable Register Partition G   */
    0x0000,        /*  Receive Channel Enable Register Partition H   */
    0x0000,        /*  Transmit Channel Enable Register Partition A   */
    0x0000,        /*  Transmit Channel Enable Register Partition B   */
    0x0000,        /*  Transmit Channel Enable Register Partition C   */
    0x0000,        /*  Transmit Channel Enable Register Partition D   */
    0x0000,        /*  Transmit Channel Enable Register Partition E   */
    0x0000,        /*  Transmit Channel Enable Register Partition F   */
    0x0000,        /*  Transmit Channel Enable Register Partition G   */
    0x0000         /*  Transmit Channel Enable Register Partition H   */
};

/* MCBSP_Config IISs
 * comment                                                 <add comments here>
 * Stop all clocks in McBSP during Peripheral Idle Mode    0x0
 * Configure DX, FSX, and CLKX as Serial Port Pins         0x1
 * Clock Mode (CLKXM)                                      External
 * SPI Clock Mode (CLKXM)                                  Slave
 * Clock Polarity (CLKXP)                                  Falling Edge
 * Frame-Sync Polarity (FSXP)                              Active High
 * DX Pin Delay (DXENA)                                    Disable
 * Transmit Delay (XDATDLY)                                0-bit
 * Phase (XPHASE)                                          Single-phase
 * Word Length Phase1 (XWDLEN1)                            32-bits
 * Word Length Phase2 (XWDLEN2)                            8-bits
 * Words/Frame Phase1 (XFRLEN1)                            0x1
 * Words/Frame Phase2 (XFRLEN2)                            0x1
 * Detect Sync Error (XSYNCERR)                            Disable
 * Interrupt Mode (XINTM)                                  XRDY
 * Early Frame Sync Response (XFIG)                        Ignore
 * Companding (XCOMPAND)                                   No Companding-MSB First
 * Transmit Frame-Sync Source                              External
 * Configure DR, FSR, CLKR, and CLKS as Serial Port Pins   0x1
 * Clock Mode (CLKRM)                                      External
 * SPI Clock Mode (CLKRM)                                  CLKX Driven
 * Clock Polarity (CLKRP)                                  Rising Edge
 * Frame-Sync Polarity (FSRP)                              Active High
 * Receive Delay (RDATDLY)                                 0-bit
 * Phases (RPHASE)                                         Single-phase
 * Word Length Phase1 (RWDLEN1)                            32-bits
 * Word Length Phase2 (RWDLEN2)                            8-bits
 * Words/Frame Phase1 (RFRLEN1)                            0x1
 * Words/Frame Phase2 (RFRLEN2)                            0x1
 * Detect Sync Error (RSYNCERR)                            Disable
 * Interrupt Mode (RINTM)                                  RRDY
 * Frame-Sync Mode (FSRM)                                  External
 * Early Frame Sync Response (RFIG)                        Ignore
 * Sign-Ext and Justification (RJUST)                      Right-justify/zero-fill
 * Companding (RCOMPAND)                                   No Companding-MSB First
 * Breakpoint Emulation                                    Stop After Current Word
 * SPI Mode (CLKSTP)                                       Disable
 * Digital Loop Back (DLB)                                 Disable
 * A-Bis (ABIS)                                            Disable
 * SRG Clock Source (CLKSM)                                CLKS Pin
 * Clock Synchronization With CLKS Pin (GSYNC)             Disable
 * CLKS Polarity Clock Edge (From CLKS Pin) (CLKSP)        Rising Edge of CLKS
 * Transmit Frame-Sync Mode (FSXM = 1) (FSGM)              Disable
 * Frame Width (1-256) (FWID)                              0x1
 * Clock Divider (1-256) (CLKGDV)                          0x1
 * Frame Period (1-4096) (FPER)                            0x1
 * Select CLKX Pin as                                      Input
 * Select FSX Pin as                                       Input
 * Select CLKR Pin as                                      Input
 * Select FSR Pin as                                       Input
 * Select DX Pin as                                        Output
 * Select DR Pin as                                        Input
 * Select CLKS Pin as                                      Input
 * Set Manually                                            0x0
 * Serial Port Control Register 1                          0x0000
 * Serial Port Control Register 2                          0x0100
 * Receive Control Register 1                              0x00a0
 * Receive Control Register 2                              0x0004
 * Transmit Control Register 1                             0x00a0
 * Transmit Control Register 2                             0x0004
 * Sample Rate Generator Register 1                        0x0000
 * Sample Rate Generator Register 2                        0x0000
 * Multichannel Control Register 1                         0x0000
 * Multichannel Control Register 2                         0x0000
 * Pin Control Register                                    0x0003
 * RX Channel Enable                                       All 128 Channels
 * Receive Partition A (RPABLK)                            Block 0. Channel 0-15
 * Receive Partition B (RPBBLK)                            Block 1. Channel 16-31
 * Receive Channel Enable Register Partition A             0x0000
 * Receive Channel Enable Register Partition B             0x0000
 * Receive Channel Enable Register Partition C             0x0000
 * Receive Channel Enable Register Partition D             0x0000
 * Receive Channel Enable Register Partition E             0x0000
 * Receive Channel Enable Register Partition F             0x0000
 * Receive Channel Enable Register Partition G             0x0000
 * Receive Channel Enable Register Partition H             0x0000
 * TX Channel Enable                                       All 128 Channels
 * Transmit Partition A (XPABLK)                           Block 0. Channel 0-15
 * Transmit Partition B (XPBBLK)                           Block 1. Channel 16-31
 * Transmit Channel Enable Register Partition A            0x0000
 * Transmit Channel Enable Register Partition B            0x0000
 * Transmit Channel Enable Register Partition C            0x0000
 * Transmit Channel Enable Register Partition D            0x0000
 * Transmit Channel Enable Register Partition E            0x0000
 * Transmit Channel Enable Register Partition F            0x0000
 * Transmit Channel Enable Register Partition G            0x0000
 * Transmit Channel Enable Register Partition H            0x0000
 * ABIS RX. Bit Enable Register Partition A                0x0000
 * ABIS RX. Bit Enable Register Partition B                0x0000
 * ABIS TX. Bit Enable Register Partition A                0x0000
 * ABIS TX. Bit Enable Register Partition B                0x0000
 */
MCBSP_Config IISs = {
    0x0000,        /*  Serial Port Control Register 1   */
    0x0100,        /*  Serial Port Control Register 2   */
    0x00a0,        /*  Receive Control Register 1   */
    0x0004,        /*  Receive Control Register 2   */
    0x00a0,        /*  Transmit Control Register 1   */
    0x0004,        /*  Transmit Control Register 2   */
    0x0000,        /*  Sample Rate Generator Register 1   */
    0x0000,        /*  Sample Rate Generator Register 2   */
    0x0000,        /*  Multichannel Control Register 1   */
    0x0000,        /*  Multichannel Control Register 2   */
    0x0003,        /*  Pin Control Register   */
    0x0000,        /*  Receive Channel Enable Register Partition A   */
    0x0000,        /*  Receive Channel Enable Register Partition B   */
    0x0000,        /*  Receive Channel Enable Register Partition C   */
    0x0000,        /*  Receive Channel Enable Register Partition D   */
    0x0000,        /*  Receive Channel Enable Register Partition E   */
    0x0000,        /*  Receive Channel Enable Register Partition F   */
    0x0000,        /*  Receive Channel Enable Register Partition G   */
    0x0000,        /*  Receive Channel Enable Register Partition H   */
    0x0000,        /*  Transmit Channel Enable Register Partition A   */
    0x0000,        /*  Transmit Channel Enable Register Partition B   */
    0x0000,        /*  Transmit Channel Enable Register Partition C   */
    0x0000,        /*  Transmit Channel Enable Register Partition D   */
    0x0000,        /*  Transmit Channel Enable Register Partition E   */
    0x0000,        /*  Transmit Channel Enable Register Partition F   */
    0x0000,        /*  Transmit Channel Enable Register Partition G   */
    0x0000         /*  Transmit Channel Enable Register Partition H   */
};

/*  Handles  */
MCBSP_Handle hMcbsp0;
MCBSP_Handle hMcbsp1;

/*
 *  ======== cslCfgInit() ========  
 */
void cslCfgInit()
{
        hMcbsp0 = MCBSP_open(MCBSP_PORT0, MCBSP_OPEN_RESET);
    hMcbsp1 = MCBSP_open(MCBSP_PORT1, MCBSP_OPEN_RESET);
        
    EMIF_config(&emifCfg0);
    MCBSP_config(hMcbsp0, &IISs);
    MCBSP_config(hMcbsp1, &IISm);

    /* You must use DMA_start() in your main code to start the DMA. */

	
    
    /* You must use MCBSP_start() in your main code to start the MCBSP. */

    
    

    
    

}
