// The code drives dual PCM3006 daughter card on VC5510 DSK
// The user should add dsk5510bslx.lib into the project
// Originally by Wei Li, liwei2000@gmail.com
// Modified by Michael Morrow for the VC5510 DSK and CCS v3.1


//#include "dsk5510_dual3006cfg.h"
#include "dsk5510_dual3006cfg_csl.h"
#include "dsk5510.h"
#include "dsk5510_led.h"
#include "csl.h"
#include "csl_irq.h"

#define FILE_INPUT
						// uncomment when you want to use input from file
						// remember to include your input in the asm file
#define count 1000		// used for file input.
						// change value to

Uint32 input[2];  // mySample[0] = channel 1 and 2
					// mySample[1] = channel3 and 4
Uint32 output[2]; // outSample[0] = channel 1 and 2
					// outSample[1] = channel 3 and 4

extern int lms();
Uint32 * inPtr;
Uint32 * outPtr;

#ifdef FILE_INPUT
unsigned int processed = 0;
extern Uint32 tv_inbuf[count];
extern Uint32 tv_inbuf1[count];
extern Uint16 tv_outbuf[count];
#endif

void myHWI();

void main()
{
	CSL_init();
	cslCfgInit();

    // Initialize the board support library, must be called first
    DSK5510_init();

    // Switch McBSP0 and McBSP1 to EPI
    DSK5510_rset(DSK5510_MISC, 0x03);// McBSP 0&1 to J3

    /* Start McBSP0 IIS slave */
    MCBSP_start(hMcbsp0, MCBSP_XMIT_START | MCBSP_RCV_START |
	MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 220);

    /* Start McBSP1 IIS master */
    MCBSP_start(hMcbsp1, MCBSP_XMIT_START | MCBSP_RCV_START |
	MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 220);

    IRQ_enable(IRQ_EVT_RINT0);

	inPtr = &input[0];
	outPtr = &output[0];

    return;
}

void HWI_RINT0()
{
    input[0] = MCBSP_read32(hMcbsp0);
    input[1] = MCBSP_read32(hMcbsp1);

	#ifdef FILE_INPUT  // Use input from file buffer
	if (processed < count)
	{
		input[0] = tv_inbuf[processed];		// un-filtered signal (x)
		input[1] = tv_inbuf1[processed];	// desired, filtered signal (d)
	}
	#endif

	int out;
	// add conditional so that wbuf[] is only changed if there are still samples to be processed
	#ifdef FILE_INPUT
	if (processed < count)
	{
	#endif
		out = lms();
	#ifdef FILE_INPUT
	}
	#endif

//	output[0] = ((Uint32)out) << 16;	// store output in both high and low 16 bits of 32-bit output variable
//	output[0] += (Uint32)out;
//	output[0] = input[0];

	#ifdef FILE_INPUT  // Save output to buffer
	if (processed < count)
	{
		//now only increase processed once each iteration
		tv_outbuf[processed++] = (Uint16)(output[0] >> 16);	// convert to 16-bit variable
	}
	#endif

    MCBSP_write32(hMcbsp0, input[0]);
	MCBSP_write32(hMcbsp1, input[1]);
}
