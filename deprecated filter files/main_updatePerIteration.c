// The code drives dual PCM3006 daughter card on VC5510 DSK
// The user should add dsk5510bslx.lib into the project
// Originally by Wei Li, liwei2000@gmail.com
// Modified by Michael Morrow for the VC5510 DSK and CCS v3.1


//#include "dsk5510_dual3006cfg.h"
#include "dsk5510_dual3006cfg_csl.h"
#include "dsk5510.h"
#include "dsk5510_led.h"
#include "csl.h"
#include "csl_irq.h"

//#define FILE_INPUT
						// uncomment when you want to use input from file
						// remember to include your input in the asm file
#define count 1000		// used for file input.
						// change value to

Uint32 input[2];  // mySample[0] = channel 1 and 2
					// mySample[1] = channel3 and 4
Uint32 output[2]; // outSample[0] = channel 1 and 2
					// outSample[1] = channel 3 and 4

#define filter_order 128
#define filterMinus 127
//unsigned short filterorder = filter_order;	// presumably 8 bits = 256
long stepsize=6553;	// should be able to fit in an int; long, so we don't have to typecast
int xbuf[filter_order];
int wbuf[filter_order];
unsigned short index=0;

int adaptiveLMS(long des);
extern int lmsupdate(int w, int e, int x);

#ifdef FILE_INPUT
unsigned int processed = 0;
extern Uint32 tv_inbuf[count];
extern Uint32 tv_inbuf1[count];
extern Uint16 tv_outbuf[count];
#endif

void myHWI();

void main()
{
	CSL_init();
	cslCfgInit();

    // Initialize the board support library, must be called first
    DSK5510_init();

    // Switch McBSP0 and McBSP1 to EPI
    DSK5510_rset(DSK5510_MISC, 0x03);// McBSP 0&1 to J3

    /* Start McBSP0 IIS slave */
    MCBSP_start(hMcbsp0, MCBSP_XMIT_START | MCBSP_RCV_START |
	MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 220);

    /* Start McBSP1 IIS master */
    MCBSP_start(hMcbsp1, MCBSP_XMIT_START | MCBSP_RCV_START |
	MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 220);

    IRQ_enable(IRQ_EVT_RINT0);

	size_t i;
	for (i=0; i<filter_order; i++)
		xbuf[i] = wbuf[i] = 0;

	//wbuf[filter_order-1]=0;	// set the first tap in the filter to 0

    return;
}

void HWI_RINT0()
{
    input[0] = MCBSP_read32(hMcbsp0);
    input[1] = MCBSP_read32(hMcbsp1);

	#ifdef FILE_INPUT  // Use input from file buffer
	if (processed < count)
	{
		input[0] = tv_inbuf[processed];		// un-filtered signal (x)
		input[1] = tv_inbuf1[processed];	// desired, filtered signal (d)
	}
	#endif

	//update xbuf
	xbuf[index]=(input[0] >> 16);
	long des=(input[1] >> 16);	// should be able to fit in an int, but we're avoiding casting

	int out;
	// add conditional so that wbuf[] is only changed if there are still samples to be processed
	#ifdef FILE_INPUT
	if (processed < count)
	{
	#endif
		out = adaptiveLMS(des);
	#ifdef FILE_INPUT
	}
	#endif

//	output[0] = ((Uint32)out) << 16;	// store output in both high and low 16 bits of 32-bit output variable
//	output[0] += (Uint32)out;
	output[0] = input[0];

	#ifdef FILE_INPUT  // Save output to buffer
	if (processed < count)
	{
		//now only increase processed once each iteration
		tv_outbuf[processed++] = (Uint16)(output[0] >> 16);	// convert to 16-bit variable
	}
	#endif

    MCBSP_write32(hMcbsp0, output[0]);
	MCBSP_write32(hMcbsp1, output[1]);
}

// saturate the input for 16 bits
int long2int(long in)
{
	if (in >= 32767)
		return 32767;
	else if (in <= -32768)
		return -32768;
	else
		return (int)in;
}

int adaptiveLMS(long des){
	long y=0;	// convolution result
	long e=0;
	unsigned short i;

	for (i=0; i<filter_order; i++)
	{
		// if filterorder is a power of 2, use bitwise &
		// wbuf[0] corresponds to last tap in the filter
		y += (long)wbuf[i]*(long)xbuf[(i+index+1) & filterMinus];
	}

	// right shift 15 bits to account for fixed point multiplication
	y = y >> 15;	// y is now Q15 as opposed to Q30

	e = des - y;

	//update w
	long temp = (stepsize * e) >> 15;
	for(i=0; i<filter_order; i++)
	{
	//	wbuf[i] = long2int(wbuf[i] + ((temp * xbuf[(i+index+1) & filterMinus]) >> 15));
		wbuf[i] = lmsUpdate(wbuf[i], (int)e, xbuf[(i+index+1) & filterMinus]);
	}

	index = (index+1) & filterMinus;
	
	// saturate and invert sign
	if (y >= 32767)
		return -32768;
	else if (y <= -32768)
		return 32767;
	else
		return -1*(int)y;
}
