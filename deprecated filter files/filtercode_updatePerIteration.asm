.ARMS_off						;enable assembler for ARMS=0
.CPL_on							;enable assembler for CPL=1
.mmregs							;enable mem mapped register names

	.global _lmsUpdate

	.copy "macro.asm"				; Copy in macro declaration

_lmsUpdate
; parameters are in 	T0, 	T1, 	AR0
;						w,		e,		x
; return w_new = w + stepsize * e * x

	ENTER_ASM						; Call macro. Prepares registers for assembly

	MOV		#0, AC0					; Clears AC0 and XAR3
	MOV		AC0, XAR3				; XAR3 needs to be cleared due to a bug

	BSET	SATD					; sets saturation on for D registers (applies to ADD and MPY)

	MOV		#0, AC1					; clear AC1
	MOV		#0, AC2					; clear AC2
	MOV		T1, HI(AC1)				; set up AC1 and AC2 for multiplication
	MOV		AR0, HI(AC2)
	MPY		AC1, AC2				; multiply high bits of AC1 and AC2, result is stored back into AC2
	MOV		#6553, AC1				; move step size to AC1 (BE SURE TO CHANGE THIS IF WE CHANGE THE STEP SIZE)
	MPY		AC1, AC2				; multiply high bits of AC1 and AC2, result is stored back into AC2
	MOV 	HI(AC2), T1				; take high bits of AC2 and put them in AC0
	ADD		T1, T0, AC0				; add T0 to T1 (w + mu*e*x), result is stored in AC0

	MOV		HI(AC0), T0				; return value as 16 bits, or AC0 is for long data values (32 bits)

	BCLR	SATD					; not sure if this is necessary

	LEAVE_ASM						; Call macro to restore registers

	RET
