	.ARMS_off						;enable assembler for ARMS=0
	.CPL_on							;enable assembler for CPL=1
	.mmregs							;enable mem mapped register names

	.global _lms
	.global _inPtr
	.global _outPtr

	.copy "macro.asm"					; Copy in macro declaration

	.sect ".data"

FIR_len1	.set 128						; This is a 128-tap filter

stepsize 	.set 6553

	.align 256						; Align to a power of 2 greater than the filter order
wBuffer .space 16*FIR_len1
;	.copy "zero.txt"


	.align 256			
xBuffer	.space 16*FIR_len1				; Allocate FIR_len1 words of storage for filter state.

new_sample_index						; Allocate storage to save index
	.word	0						; in firState

	.copy "testvect.asm"

	.sect ".text2"


_lms
	; convolve xbuf and wbuf to get y
	; find the error between y and des
	; update wbuf with: 	w_new = w + stepsize * e * x
	; increment index for xbuf (or do this in C)
	; saturate and return y in T0

	; x is in HI(input[0])
	; d is in HI(input[1])

	ENTER_ASM						; Call macro. Prepares registers for assembly
	
	MOV		#0, AC0					; Clears AC0 and XAR3
	MOV		AC0, XAR3				; XAR3 needs to be cleared due to a bug

	MOV		dbl (*(#_inPtr)), XAR6			; XAR6 contains address to input
	MOV		dbl (*(#_outPtr)), XAR7			; AR7 contains address to output

	BSET		AR2LC					; sets circular addressing for AR2
	BSET		AR1LC					; sets circular addressing for AR1
	MOV		#FIR_len1, BK03				; initialize circular buffer length for register 0-3
;	BSET 	SXMD

	MOV		#xBuffer, AR2			; State pointer is in AR2
	MOV		mmap(AR2), BSA23			; BSA23 contains address of xBuffer
	MOV		#new_sample_index, AR4			; State index pointer is in AR4
	MOV		*AR4, AR2				; AR2 contains the index of oldest state
	
	MOV		#wBuffer, AR1				; initialize coefficient pointer
	MOV 	mmap(AR1), BSA01			; BSA01 contains address of wBuffer

	MOV		*AR6+ << #16, AC0			; Receive ch1 into AC0 accumulator
	MOV		AC0, AC1				; Transfer AC0 into AC1 for safekeeping
	
	MOV		HI(AC0), *AR2+				; store current input into state buffer
	MOV		#0, AC0					; Clear AC0

	BSET 	SATD					; saturate for MACM

	MOV 	#0, AR1					; clear AR1 so that circular addressing actually works

	RPT		#FIR_len1-1				; Repeat next instruction FIR_len1 times
	MACM	*AR1+,*AR2+,AC0,AC0			; multiply coef. by state & accumulate
	round	AC0					; Round off value in 'AC0' to 16 bits  (contains y)

	MOV		HI(AC0), *AR7+				; Store filter output (from AC0) into ch1 of both outputs
	MOV		#0, *AR7+				; Store saved input (from AC1) into ch2 of both outputs
	MOV		HI(AC0), *AR7+
	MOV		#0, *AR7+

	MOV		AR2, *AR4				; Save the index of the latest state back into new_sample_index

	MOV 	HI(AC0), T0				; T0 (return register) now contains y

	AMAR 	*AR6+					; increment AR6 so that HI(input[1]) is now available
									; (this should be the desired signal)

	SUB 	*AR6 << #16, AC0, AC0	; AC0 now contains e=d-y

	MOV 	#stepsize, T1			; T1 now contains the stepsize
	MPY 	T1, AC0, AC0			; AC0 now contains e*stepsize
	round 	AC0
	MOV 	HI(AC0), T1				; T1 now contains e*stepsize

	MOV		#FIR_len1-1, BRC0		; Repeat next instruction block FIR_len1 times
	RPTB 	#end_of_update-1		; repeat until end_of_update label
	MOV 	*AR1 << #16, AC0 	; move w value into AC0
	MACM	*AR2+, T1, AC0, AC0	; AC0 contains the new wbuf coefficient (w_new = w_old + e*stepsize*x)
	round 	AC0
	MOV 	HI(AC0), *AR1+		; replace the wbuf coefficient with the new one
end_of_update:

	LEAVE_ASM						; Call macro to restore registers

	RET
